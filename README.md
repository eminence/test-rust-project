# Test Rust Project

This project demonstrates a gitlab configuration that will capture various code quality metrics and surface them via specific gitlab interfaces.

To see this in action, look at the [open merge request](https://gitlab.com/eminence/test-rust-project/-/merge_requests/2).

The [`.gitlab-ci.yml`](https://gitlab.com/eminence/test-rust-project/-/blob/main/.gitlab-ci.yml) configuration for this project will do 3 things:

* Use [grcov](https://github.com/mozilla/grcov) and rust's ability to generate [llvm soruce-based code coverage](https://rustc-dev-guide.rust-lang.org/llvm-coverage-instrumentation.html) to generate code coverage information when running tests.  This data is exposed using gitlab's [test coverage visualization](https://docs.gitlab.com/ee/user/project/merge_requests/test_coverage_visualization.html) feature.
* Use the [GitLab Clippy](https://gitlab.com/dlalic/gitlab-clippy) tool to convert [clippy](https://github.com/rust-lang/rust-clippy) errors/warnings into data for gitlab's  [code quality](https://docs.gitlab.com/ee/user/project/merge_requests/code_quality.html) widget
* Use the [cargo2junit](https://github.com/johnterickson/cargo2junit) tool to convert cargo test results into junit files which gitlab displays in [unit test widget](https://docs.gitlab.com/ee/ci/unit_test_reports.html).



Also, within the project CI settings, there is a field for setting a coverage regular expression, to extract an overall coverage percentage from a CI job log.  It is set to:

    coverage=(\d+\.\d+%)
