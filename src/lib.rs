
pub fn run(b: bool) -> u8 {
    let mut x = 0;
    if b {
        x += 10
    }

    x += 20;

    x
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn it_works() {
        assert_eq!(run(false), 20);
    }
}
